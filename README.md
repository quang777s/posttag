posttag
=======


SETUP PROJECT

Created your parameter.yml in app/config/ (parameters.yml.dist)
Run below command in posttag folder:
app/console doctrine:database:create
app/console doctrine:schema:update --force 

Download composer.phar:
curl -sS https://getcomposer.org/installer | php

And run command to update:
php composer.phar update


THE API

Add new post
METHOD: POST
URL: {hostname}/app_dev.php/api/v1/posts/add
FORM-DATA: post_title, post_body, tags
Example:
post_title: "the title",
post_body: "the body",
tags: "book, ebook, english"

View a post
METHOD: GET
URL: {hostname}/app_dev.php/api/v1/posts/read?post_id={id}

Edit post
METHOD: POST
URL: {hostname}/app_dev.php/api/v1/posts/edit
FORM-DATA: post_title, post_body, tags
Example:
post_title: "updated title",
post_body: "the body",
tags: "ebook, english, culture"

Delete a post
METHOD: DELETE
URL: {hostname}/app_dev.php/api/v1/posts/delete?post_id={id}

Add new tag
METHOD: POST
URL: {hostname}/app_dev.php/api/v1/tags/add
FORM-DATA: tag_name

View a tag
METHOD: GET
URL: {hostname}/app_dev.php/api/v1/tags/read?tag_id={id}

Edit tag
METHOD: POST
URL: {hostname}/app_dev.php/api/v1/tags/edit
FORM-DATA: tag_name, tag_id

Delete a tag
METHOD: DELETE
URL: {hostname}/app_dev.php/api/v1/tags/delete?tag_id={id}

Select posts by tag or tags
METHOD: GET
URL: {hostname}/app_dev.php/api/v1/posts/select?tags={tags}
Example:
tags: book, ebook, english

Count posts by tag or tags
METHOD: GET
URL: {hostname}/app_dev.php/api/v1/posts/count?tags={tags}
Example:
tags: book, ebook, english