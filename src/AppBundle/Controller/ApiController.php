<?php

namespace AppBundle\Controller;

use FOS\RestBundle\Controller\FOSRestController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use AppBundle\Entity\Post;
use AppBundle\Entity\Tag;

use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;

class ApiController extends FOSRestController
{
	public function addPostAction(Request $request)
    {
     	$postTitle = $request->request->get('post_title');
     	$postBody = $request->request->get('post_body');
     	$tags = $request->request->get('tags');
     	$result = null;

     	if ($postTitle != null) {
     		$em = $this->getDoctrine()->getManager();
     		$entity = new Post();
     		$entity->setTitle($postTitle);
     		$entity->setBody($postBody);

     		if ($tags != null) {
     			$tagArray = explode(',', $tags);
     			foreach ($tagArray as $key => $tag) {
     				$tag = trim($tag);
     				$tagEntity = $em->getRepository('AppBundle:Tag')->findOneByName($tag);

     				if ($tagEntity instanceof Tag) {
     					$entity->addTag($tagEntity);
     				} else {
     					$tagEntity = new Tag;
     					$tagEntity->setName($tag);
     					$em->persist($tagEntity);
     				}
     			}
     		}
     		
            $em->persist($entity);
            $em->flush();

            return new JsonResponse($entity->toArray());
     	}

     	return new Response($result);
    }

    /**
     * Read a Post entity.
     *
     * @Route("/api/v1/posts/read", name="api_1_read_post")
     * @Method("GET")
     */
    public function readPostAction(Request $request)
    {
    	$response = new JsonResponse();
        $response->setMaxAge(3600);
        $response->setSharedMaxAge(3600);
        $response->setPublic();
        $postId = $request->query->get('post_id');
        $result = null;

        if ($postId != null) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('AppBundle:Post')->find($postId);

            if (!$entity) {
                return new Response('Unable to find Post entity.');
            }

            $response->setData($entity->toArray());
        }

        return $response;
    }

    public function editPostAction(Request $request)
    {
        $postId = $request->request->get('post_id');
        $postTitle = $request->request->get('post_title');
        $postBody = $request->request->get('post_body');
        $tags = $request->request->get('tags');
        $result = null;

        if ($postId != null) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('AppBundle:Post')->find($postId);

            if (!$entity) {
                return new Response('Unable to find Post entity.');
            }

            $entity->setTitle($postTitle);
            $entity->setBody($postBody);

            if ($tags != null) {
            	$entity->removeTags();
     			$tagArray = explode(',', $tags);
     			foreach ($tagArray as $key => $tag) {
     				$tag = trim($tag);
     				$tagEntity = $em->getRepository('AppBundle:Tag')->findOneByName($tag);

     				if ($tagEntity instanceof Tag) {
     					$entity->addTag($tagEntity);
     				} else {
     					$tagEntity = new Tag;
     					$tagEntity->setName($tag);
     					$em->persist($tagEntity);
     				}
     			}
     		} else {
     			$entity->removeTags();
     		}

            $em->persist($entity);
            $em->flush();

            return new JsonResponse($entity->toArray());
        }

        return new Response($result);
    }

     /**
     * Deletes a Post entity.
     *
     * @Route("/api/v1/posts/delete", name="api_1_delete_post")
     * @Method("DELETE")
     */
    public function deletePostAction(Request $request)
    {
        $postId = $request->query->get('post_id');

        if ($postId != null) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('AppBundle:Post')->find($postId);

            if (!$entity) {
                return new Response('Unable to find Post entity.');
            }

            $em->remove($entity);
            $em->flush();
            return new Response('success');
        }

        return new Response('error');
    }

    public function addTagAction(Request $request)
    {
        $tagName = $request->request->get('tag_name');
        $result = null;

        if ($tagName != null) {
            $entity = new Tag();
            $entity->setName($tagName);
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            return new JsonResponse($entity->toMiniArray());
        }

        return new Response($result);
    }

    /**
     * Read a Tag entity.
     *
     * @Route("/api/v1/tags/read", name="api_1_read_tag")
     * @Method("GET")
     */
    public function readTagAction(Request $request)
    {
        $tagId = $request->query->get('tag_id');
        $result = null;

        if ($tagId != null) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('AppBundle:Tag')->find($tagId);

            if (!$entity) {
                return new Response('Unable to find Tag entity.');
            }

            return new JsonResponse($entity->toMiniArray());
        }

        return new Response($result);
    }

    public function editTagAction(Request $request)
    {
        $tagId = $request->request->get('tag_id');
        $tagName = $request->request->get('tag_name');
        $result = null;

        if ($tagId != null) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('AppBundle:Tag')->find($tagId);

            if (!$entity) {
                return new Response('Unable to find Tag entity.');
            }

            $entity->setName($tagName);         
            $em->persist($entity);
            $em->flush();

            return new JsonResponse($entity->toMiniArray());
        }

        return new Response($result);
    }

    /**
     * Deletes a Tag entity.
     *
     * @Route("/api/v1/tags/delete", name="api_1_delete_tag")
     * @Method("DELETE")
     */
    public function deleteTagAction(Request $request)
    {
        $tagId = $request->query->get('tag_id');

        if ($tagId != null) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('AppBundle:Tag')->find($tagId);

            if (!$entity) {
                return new Response('Unable to find Tag entity.');
            }

            $em->remove($entity);
            $em->flush();
            return new Response('success');
        }

        return new Response('error');
    }

    /**
     * Select Post by tag or tags.
     *
     * @Route("/api/v1/posts/select", name="api_1_select_post")
     * @Method("GET")
     */
    public function selectPostAction(Request $request)
    {
    	$response = new JsonResponse();
        $response->setMaxAge(3600);
        $response->setSharedMaxAge(3600);
        $response->setPublic();

        $tags = $request->query->get('tags');
        $result = array();

        if ($tags != null) {
        	$em = $this->getDoctrine()->getManager();
        	$tagArray = explode(',', $tags);
 			foreach ($tagArray as $key => $tag) {
 				$tag = trim($tag);
 				$tagEntity = $em->getRepository('AppBundle:Tag')->findOneByName($tag);

 				if ($tagEntity instanceof Tag) {
 					$posts = $tagEntity->getPosts();

 					foreach ($posts as $post) {
 						if (!in_array($post->toMiniArray(), $result)) {
	 						$result[] = $post->toMiniArray();
	 					}
 					}
 				}
 			}
 			$response->setData($result);
        }

        return $response;
    }

     /**
     * Count Post by tag or tags.
     *
     * @Route("/api/v1/posts/count", name="api_1_count_post")
     * @Method("GET")
     */
    public function countPostAction(Request $request)
    {
    	$response = new JsonResponse();
        $response->setMaxAge(3600);
        $response->setSharedMaxAge(3600);
        $response->setPublic();

        $tags = $request->query->get('tags');
        $result = array();

        if ($tags != null) {
        	$em = $this->getDoctrine()->getManager();
        	$tagArray = explode(',', $tags);
 			foreach ($tagArray as $key => $tag) {
 				$tag = trim($tag);
 				$tagEntity = $em->getRepository('AppBundle:Tag')->findOneByName($tag);

 				if ($tagEntity instanceof Tag) {
 					$posts = $tagEntity->getPosts();

 					foreach ($posts as $post) {
 						if (!in_array($post, $result)) {
	 						$result[] = $post;
	 					}
 					}
 				}
 			}
            $response->setData(count($result));
        }

        return $response;
    }
}